<?php

header("Content-Type: application/json; charset=UTF-8");
header( "Access-Control-Allow-Origin: *");

require 'db.php';
global $db, $db_table;

	$result = $db->get_results( "SELECT DISTINCT `teacher_name` FROM `$db_table` WHERE `teacher_name` != '' ORDER BY `teacher_name` ASC", ARRAY_A );

	if ($result) {
		$teachers = [ ];
		foreach ( $result as $teacher ) {
			$teachers[] = $teacher["teacher_name"];
		}

		$outp = json_encode( $teachers );
		echo $outp;
	} else {
		echo "Error";
	}

exit;