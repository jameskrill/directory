<?php

header("Content-Type: application/json; charset=UTF-8");
header( "Access-Control-Allow-Origin: *");

require 'db.php';
global $db, $db_table;

	$result = $db->get_results( "SELECT DISTINCT `grade` FROM `$db_table` ORDER BY `grade` ASC", ARRAY_A );

	if ($result) {
		$grades = [ ];
		foreach ( $result as $grade ) {
			$grades[] = $grade["grade"];
		}

		$outp = json_encode( $grades );
		echo $outp;
	} else {
		echo "Error";
	}


die();