<?php

header("Content-Type: application/json");
header( "Access-Control-Allow-Origin: *");

require 'db.php';
global $db, $db_table;

	$result = $db->get_results( "SELECT * FROM `$db_table` ORDER BY `student_name` ASC", ARRAY_A );

	if ($result) {
		echo json_encode( $result );
	} else {
		echo json_encode(array());
	}

die();