<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type");
header("Content-Type: application/json");
require 'db.php';

global $db, $db_table;

// Angular POST fix
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if ( isset( $request->action ) && $request->action == "search" ) {

	$query_string = "SELECT * FROM `$db_table`";

	$where_string = "";
	if ( isset( $request->where ) ) {

		$where_string = " WHERE";


		$o = 0;
		foreach ( $request->where as $where ) {

			if ( isset( $where->queries ) ) {

				if ( count( (array) $where->queries ) > 1 ) {
					$where_string .= " (";
				}

//				print_r($where->queries);
				foreach ( $where->queries as $queries ) {
					if ( $o != 0 ) {
						$where_string .= " " . $where->clause . " (";
					}
					$i = 0;
					$queryArray = (array) $queries;
					$whereClause = count( $queryArray ) > 1 ? "AND" : "OR";
					foreach ( $queries as $key => $value ) {
						if ( $i == 0 ) {
							$where_string .= " `" . $key . "` " . $where->operator . " '" . $db->escape($value) . "'";
						} else {
							$where_string .= " " . $whereClause . " `" . $key . "` " . $where->operator . " '" . $db->escape($value) . "'";
						}
						$i ++;
					}
					if ( $o != 0 ) {
						$where_string .= " )";
					}
					$o++;
				}

				if ( count( (array) $where->queries ) > 1 ) {
					$where_string .= " )";
				}

			}


		}
		$query_string .= $where_string;
	}

	$orderby = "";
	$orderby = " ORDER BY `student_name` ASC";

	$query_string .= $orderby;

	// The query
	$result = $db->get_results( $query_string, ARRAY_A );

	if ( $result ) {
		$outp = json_encode( $result );
	} else {
		$outp = json_encode(array());
	}

	echo( $outp );
	exit;

} else if ( isset( $request->action ) && $request->action == "get_student_rows" ) {

	$query_string = "SELECT * FROM `$db_table` WHERE `student_name` = '{$db->escape($request->name)}'";

	$result = $db->get_results( $query_string, ARRAY_A );

	if ( $result ) {
		$outp = json_encode( $result );
	} else {
		$outp = "Error";
	}

	echo( $outp );
	exit;

} else if ( isset( $request->action ) && $request->action == "update_student_data" ) {

	if ( isset($request->studentData) && !empty($request->studentData) ){

			$id = $request->studentData->id;
			unset($request->studentData->id);

			$query_string = "UPDATE `$db_table` SET";

			foreach($request->studentData as $key=>$value) {
//				$key = str_replace( '_', ' ', $key );
				$query_string .= " `$key` = '{$db->escape($value)}',";
			}

			$query_string = substr($query_string, 0, -1);

			$query_string .= " WHERE `id` = ".$id;

			error_log($query_string);

			$result = $db->query($query_string);

			if (!empty($result)) {
				$result = $result[0];
				$result['success'] = true;
			} else {
				$result = array(
					'success' => false
				);
			}

	} else {
		$result = array(
			'success' => false
		);
	}

	$outp = json_encode( $result );
	echo( $outp );
	exit;

} else {
	die('No Access');
}

?>
