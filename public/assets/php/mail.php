<?php
/**
 * Created by IntelliJ IDEA.
 * User: jimkrill
 * Date: 8/29/15
 * Time: 12:39 PM
 */

 header("Access-Control-Allow-Origin: *");
 header("Access-Control-Allow-Headers: content-type");
 header("Content-Type: application/json");

require "vendor/phpmailer/PHPMailerAutoload.php";
require '../../data/db.php';

global $db, $db_table;

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

// Emailer Settings
$mail = new PHPMailer;

$mail->isSMTP();
$mail->Host = 'box1141.bluehost.com';
$mail->SMTPAuth = true;
$mail->SMTPSecure = "ssl";
$mail->Username = "directory@woodstockelementary.com";
$mail->Password = "2ep+s<M1.SQj";
$mail->Port = 465;

$mail->From = "directory@woodstockelementary.com";
$mail->FromName = "Woodstock Elementary School";

// Email Switch
if ( isset($request->action) && $request->action == "request_password" ) {

	// Email Password Form

	$email = $request->email;

	// Connect to database to see if the email exists
	$query_string = "SELECT * FROM `$db_table` WHERE `p1_email` = '".$email."' OR `p2_email` = '".$email."' OR `p3_email` = '".$email."'";

	$result = $db->get_results( $query_string );
	if ($result) {
		$mail->addAddress($email);

		$mail->Subject = "Woodstock Elementary School Online Directory Password Request";
		$mail->Body = "The Woodstock Elementary School Online Directory password is: WSLions5601";

		if (!$mail->send()) {
			echo 'Message could not be sent.';
		} else {
			echo "The password has been emailed to the email address you entered.";
		}
	} else {
		$mail->addAddress("jameskennedykrill@gmail.com");
		$mail->addCC('admin@woodstockelementary.com');

		$mail->Subject = "Woodstock Elementary School Online Directory Password Request";
		$mail->Body = "The following email address has requested the password for the Online Directory: ".$email;

		if (!$mail->send()) {
			echo 'Message could not be sent.';
		} else {
			echo "Your email was not found in the directory. An email has been sent to the administration and someone will be in touch with you shortly. Thank you.";
		}
	}

	exit;

} else if ( isset($request->action) && $request->action == "request_correction" ) {

	// Request Correction Form
	$name = $request->name;
	$email = $request->email;
	$studentName = $request->studentName;
	$message = $request->message;

//	$mail->From = $email;
//	$mail->FromName = $name;

	$mail->addAddress("jameskennedykrill@gmail.com");
	$mail->addCC('admin@woodstockelementary.com');
	$mail->addBCC('sleining@pps.net');
	$mail->addBCC('trhodes1@pps.net');

	$mail->Subject = "Woodstock Elementary School Online Directory Correction Request";
	$mail->Body = "From: ".$name."\r\n\rEmail: ".$email."\r\n\rStudent Name: ".$studentName."\r\n\r".$message."\r\n\r----------------------\r\n\rJim Krill will fix the information requested on the online directory. This email is for reference only.";

	if ($request->testing) {
		echo "Test completed successfully.";
		exit;
	}

	if (!$mail->send()) {
		echo 'Request could not be sent.';
		error_log(print_r($mail->ErrorInfo,true));
	} else {
		echo "Your request has been received and will be evaluated. We will be in touch. Thank you.";
	}

	exit;

} else {

	echo "Not Authorized.";
	exit;

}
