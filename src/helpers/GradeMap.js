const GradeMap = {
  "KG": "Kindergarten",
  1: "1st grade",
  2: "2nd grade",
  3: "3rd grade",
  4: "4th grade",
  5: "5th grade"
};

export default GradeMap;
