export function reverseName(name, separator = ",", space = " ") {
  const nameParts = name.split(separator);
  let nameLength = nameParts.length;
  let reversedName = nameParts[nameLength-1].trim();
  for(let i = 0; i<nameLength-1; i++) {
    reversedName += space + nameParts[i];
  }
  return reversedName;
}

export function gradeMap(grade) {
  switch (grade) {
    case 'KG':
    case 'K':
      return 'Kindergarten';
    case '1':
    case '01':
      return '1st Grade';
    case '2':
    case '02':
      return '2nd Grade';
    case '3':
    case '03':
      return '3rd Grade';
    case '4':
    case '04':
      return '4th Grade';
    case '5':
    case '05':
      return '5th Grade';
    default:
      return grade;
  }
}
