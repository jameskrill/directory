import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router } from 'react-router-dom'
import { CookiesProvider } from 'react-cookie';

import App from './components/App';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './assets/styles/index.css';

/*
TODO
X show adult toggle
X mobile menu gets hidden behind jump nav bar
X jump to select on mobile
X login screen
X login/logout functionality
X request correction form Submit
X edit student form
X edit student form submit
- Request Password form
- Request Password form submit
*/

class Root extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <CookiesProvider>
          <App />
        </CookiesProvider>
      </div>
      </Router>
    );
  }
}

ReactDOM.render(<Root />, document.getElementById('root'));
// registerServiceWorker();
