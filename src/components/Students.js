import React, { Component } from "react";
// import WayPoint from "react-waypoint";
import { gradeMap } from '../helpers/helper_functions';
import Alpha from './Alpha';
import '../assets/styles/Students.css';

class Students extends Component {

  render() {
    const students = this.props.sortedStudents;
    const loading = this.props.loading;
    const search = this.props.search;

    if (!students && !loading) {
      // no student data and not loading, must be sitting still...
      return (

        <div className="row no-search">
          <div className="col-xs-12 col-sm-8 col-sm-offset-2">
            <div className="panel panel-primary">
              <div className="panel-heading">
                <h3 className="panel-title">Instructions:</h3>
              </div>
              <div className="panel-body">
                <p>Use the search or dropdown filters above to find families and individuals in the school directory.</p>
                <p>Please verify that your information is correct.</p>
                <p><em>If there is an error, use the <strong>"Request a Correction"</strong> link located under your child's entry, and use the form to submit a correction.</em></p>
                <p>Direct any questions or issues to <a href="mailto:jimkrill@gmail.com">Jim Krill</a>.</p>
              </div>
            </div>
          </div>
        </div>

      );
    }

    if (!students && loading) {
      return (
        <div className="loading-container">
          <div className="loader"></div>
        </div>
      );
    }

    return (
      <div className="search-results">
        <div className="container" role="main">

          {search && students && (
            <div className="row">
              <div className="col-xs-10 col-xs-offset-1">

                {search.term && (
                  <h2 className="text-center">Search Terms:
                    {' '}
                    {search.term.split(' ').map((term, i) =>
                        <span className="label label-default term" key={term+"-"+i}>
                          {term}
                        </span>
                    )}
                  </h2>
                )}

                {search.grade_filter && (
                  <h2 className="text-center">
                    Grade: {gradeMap(search.grade_filter)}
                  </h2>
                )}

                {search.teacher_filter && (
                  <h2 className="text-center">
                    Teacher: {search.teacher_filter}
                  </h2>
                )}

                {this.props.students && (
                  <h5 className="text-center">
                    Total Results: {this.props.students.length}
                  </h5>
                )}

              </div>
            </div>
          )}

          {students && Object.keys(students).map(key =>
            <Alpha
              key={key}
              alpha={students[key].alpha}
              students={students[key].students}
              currentUser={this.props.currentUser} />
          )}

        </div>
      </div>
    )
  }

}

export default Students;
