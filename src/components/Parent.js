import React, { Component } from 'react';

export default class Parent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parent: props.parent
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.parent !== this.state.parent) {
      this.setState({parent: nextProps.parent});
    }
  }

  render() {
    const parent = this.state.parent;

    return (
      <div className="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">{parent.name}</h3>
          </div>
          <div className="panel-body">
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-6">
                {parent.email && parent.showEmail && (
                  <p className="parent_email"><i className="fa fa-envelope" /> <a href={`mailto:${parent.email}`}>{parent.email}</a></p>
                )}
                {parent.address && parent.showAddress && (
                  <p>{parent.address}<br/>{parent.cityStateZip}</p>
                )}
              </div>
              <div className="col-xs-12 col-sm-12 col-md-6 text-right">
                {parent.phone && parent.showPhone && (
                  <ul className="phone-numbers">
                    <li>
                      <div>
                        <i className={`fa fa-custom-${parent.phoneType}`} /> {parent.phone}</div>
                    </li>
                  </ul>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
