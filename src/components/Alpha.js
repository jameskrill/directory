import React, { Component } from 'react';
import Student from './Student';

export default class Alpha extends Component {
  render() {
    const students = this.props.students;
    const alpha = this.props.alpha;

    return (
      <div className="alpha-section">
        <div className="anchor" id={alpha} />
        <h2 className="alpha-heading">{alpha}</h2>
        {students && Object.keys(students).map(key =>
          <Student key={key} student={students[key]} currentUser={this.props.currentUser} />
        )}
      </div>
    )
  }
}
