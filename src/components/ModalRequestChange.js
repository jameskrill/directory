import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import FormRequestChange from './FormRequestChange';

export default class ModalRequestChange extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  handleOpenModal = () => {
    this.setState({open: true});
  }

  handleCloseModal = () => {
    this.setState({ open: false });
  }

  render() {
    const { open } = this.state;
    return (
      <div>
        <p>
          <button className="btn btn-link" onClick={this.handleOpenModal}>
            <i className="fa fa-pencil" /> Request a Correction
          </button>
        </p>
        <Modal open={open} onClose={this.handleCloseModal} styles={{overlay: {zIndex: 2040}}}>
          <FormRequestChange student={this.props.student} closeModal={this.handleCloseModal} />
        </Modal>
      </div>
    )
  }
}
