import React, { Component } from 'react';

export default class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      students: null
    }
  }

  componentWillReceiveProps(nextProps) {

  }

  render() {
    let alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    return (
      <div className="navbar navbar-default navbar-fixed-top alpha-nav">
        <div className="container">
          <div className="text-center alpha-list">
            <ul className="list-inline hidden-xs hidden-sm hidden-md">
              <li>Jump to: </li>
              <li><a href="#search" className="btn btn-link btn-xs">SEARCH</a></li>
              {alphabet.map(alpha =>
                <li key={alpha}>
                  {this.props.sortedStudents[alpha] && (
                    <a href={`#${alpha}`} className="btn btn-default btn-sm">{alpha}</a>
                  )}

                </li>
              )}
            </ul>
            <div className="visible-xs-block visible-sm-block visible-md-block">
              <div className="form-group">
                <select name="alpha-select" id="alpha-select" className="form-control" onChange={(e)=>{window.location.hash = "#"+e.target.value}}>
                  <option>-- Jump To --</option>
                  <option value="top">Search (top)</option>
                  {alphabet.map((alpha) => {
                    let option = "";
                    if (this.props.sortedStudents[alpha]) {
                      option = <option key={alpha} value={alpha}>{alpha}</option>;
                    }
                    return option;
                  }
                  )}
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
