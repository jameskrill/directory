import React, { Component } from 'react';
import axios from 'axios';
import jQuery from 'jquery';
import { reverseName } from '../helpers/helper_functions';

export default class FormRequestChange extends Component {
  constructor(props) {
    super(props);

    this.state = {
      action: 'request_correction',
      testing: false,
      name: '',
      email: '',
      studentName: '',
      message: '',
      sending: false
    };
  }

  componentDidMount() {
    this.setState({studentName: reverseName(this.props.student.student_name)});
  }

  handleChange = (e) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({[name]: value});
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.setState({sending: true});

    // validation
    if (!this.state.name || !this.state.email || !this.state.studentName || !this.state.message) {
      this.setState({sending: false});
      alert('All fields are required. Please fill them out and push Submit again.');
      return;
    }

    // submission
    axios({
      url: 'http://directory.woodstockelementary.com/assets/php/mail.php',
      method: 'post',
      data: this.state
    }).then(response => {
      let form = jQuery('#requestChange');
      form.find('.modal-body').html('<p>' + response.data + '</p>');
    }).catch(error => {
      alert('There was an error sending this request. Please try again later.');
      this.setState({sending: false});
    });
  }

  render() {
    return (
      <form id="requestChange" name="requestPasswordForm" onSubmit={this.handleSubmit}>
          <div className="modal-header">
              <h4 className="modal-title" id="requestChangeLabel">Request a Correction</h4>
          </div>
          <div className="modal-body">
            {!this.state.sending && (
              <div>
              <div className="form-group">
                  <p>
                      Use the form below to submit a correction or change. Please be specific about what you are requesting and reference your relation to the student. Thank you.
                  </p>
              </div>
              <div className="form-group">
                  <label htmlFor="name" className="control-label">Your Name:</label>
                  <input type="text" className="form-control" id="name" name="name" value={this.state.name} onChange={this.handleChange} />
              </div>
              <div className="form-group">
                  <label htmlFor="email" className="control-label">Your Email:</label>
                  <input type="text" className="form-control" id="email" name="email" value={this.state.email} onChange={this.handleChange} />
              </div>
              <div className="form-group">
                  <label htmlFor="student-name" className="control-label">Students Name:</label>
                  <input type="text" className="form-control" id="studentName" name="studentName" value={this.state.studentName} onChange={this.handleChange} />
              </div>
              <div className="form-group">
                  <label htmlFor="message-text" className="control-label">Information to be corrected/changed:</label>
                  <textarea className="form-control" id="message" name="message" value={this.state.message} onChange={this.handleChange} />
              </div>
              </div>
            )}
            {this.state.sending && (
              <div className="loading-container">
                <div className="loader" />
              </div>
            )}

          </div>
          <div className="modal-footer">
              <p><strong>Important Note: </strong>You must fill out a form in the school office in order to update your information with the district. This form only updates information in the online directory for this school year.</p>
              <button type="button" className="btn btn-default" onClick={this.props.closeModal}>Close</button>
              {!this.state.sending && (
                <button type="submit" className="btn btn-primary" id="action" name="action" value="request_correction">Send Request</button>
              )}
          </div>
      </form>
    )
  }
}
