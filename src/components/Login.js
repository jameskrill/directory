import React, {Component} from 'react';
import Modal from 'react-responsive-modal';
import axios from 'axios';
import jQuery from 'jquery';
import FormRequestPassword from './FormRequestPassword';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      remember: false,
      loading: false,
      open: false
    };
  }

  handleLoginAttempt = (e) => {
    e.preventDefault();
    this.setState({loading: true});

    axios({
      url: 'http://directory.woodstockelementary.com/data/user_login.php',
      method: 'post',
      data: {
        password: this.state.password
      }
    })
      .then(response => {
        if (response.status === 200 && response.statusText === "OK") {
          if (response.data.success) {
            // Success
            const currentUser = {
              id: response.data.id,
              userRole: response.data.role
            };
            this.props.onLogin(currentUser, this.state.remember);
          } else {

            // Bad Password
            this.setState({
              password: "",
              loading: false
            });
            jQuery('#password').css('border', '1px solid red').attr('placeholder', 'Try again!');
            jQuery('#password').addClass('invalid');
            window.setTimeout(function () {
              jQuery('#password').removeClass('invalid');
            }, 1000);

          }
        } else {
          this.setState({
            loading: false
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  }

  handleInputChange = (e) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleOpenModal = () => {
    this.setState({open: true});
  }

  handleCloseModal = () => {
    this.setState({open: false});
  }

  render() {
    const {open} = this.state;

    return (
      <div>
        <div className="login-container">
          <div className="container search login">
            <div className="row">
              <div className="col-xs-12 col-sm-6 col-sm-offset-3">
                <form className="form-horizontal" name="loginForm" onSubmit={this.handleLoginAttempt}>

                  <div className="row">
                    <div className="col-xs-12">
                      <h2 className="app-title">
                        Woodstock <span className="hidden-xs hidden-sm">School&nbsp;</span>
                        Online Directory <span className="meta">2017 - 2018</span>
                      </h2>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-xs-12 col-md-9">
                      <label htmlFor="password" className="sr-only">Password</label>
                      <p><input type="password"
                                id="password"
                                name="password"
                                value={this.state.password}
                                onChange={this.handleInputChange}
                                placeholder="Please enter the password..."
                                className="form-control input-lg"
                                autoFocus/></p>
                    </div>
                    <div className="col-xs-12 col-md-3">
                      <p>
                        {!this.state.loading && (
                          <button type="submit" className="btn btn-primary btn-lg">Login</button>
                        )}
                        {this.state.loading && (
                          <button type="submit" className="btn btn-primary btn-lg" disabled>Loading...</button>
                        )}
                      </p>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-xs-12 remember-me-container">
                      <p>
                        <input type="checkbox"
                               name="remember"
                               id="remember"
                               checked={this.state.remember}
                               onChange={this.handleInputChange}
                               data-toggle="tooltip"
                               data-placement="bottom"
                               title="This feature uses cookies."/>
                        <small>Remember me on this device.</small>
                      </p>
                    </div>
                  </div>

                </form>

                <div className="row">
                  <div className="col-xs-10 col-xs-offset-1 request-password-container text-center">
                    <p>
                      <button className="btn btn-link" onClick={this.handleOpenModal}>
                        <i className="fa fa-key"/> Request Password
                      </button>
                    </p>
                    <Modal open={open} onClose={this.handleCloseModal} styles={{overlay: {zIndex: 1040}}} little>
                      <FormRequestPassword onCloseModal={this.handleCloseModal}/>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="search-results">
          <div className="container" role="main">
            <div className="row no-search">
              <div className="col-xs-12 col-sm-6 col-sm-offset-3">
                <div className="panel panel-primary">
                  <div className="panel-heading">
                    <h3 className="panel-title">Please note:</h3>
                  </div>
                  <div className="panel-body">
                    <h3>Older Browsers</h3>
                    <p>This tool is optomized to work best in a modern browser, such as:</p>
                    <ul>
                      <li>Internet Explorer 10+</li>
                      <li>Google Chrome</li>
                      <li>Firefox</li>
                    </ul>
                    <p>If you are using an older version of Internet Explorer, the directory may not function properly.
                      Please upgrade to a modern browser if you can.</p>

                    <h3>Bookmark this page!</h3>
                    <p>On some mobile devices (phones and tablets) you can save this page to your home screen for easy
                      access.</p>
                    <p>In most browsers, you can save this page as a bookmark or favorite.</p>

                    <h3>Save the password</h3>
                    <p>To avoid entering the password everytime you visit the directory, check the checkbox below the
                      password field to remain logged in (only applies to the current browser/device you are using).</p>
                    <p>To "log out", click Logout in the top right of the screen after logging in.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default Login;
