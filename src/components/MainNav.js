import React, { Component } from "react";
import { Link } from 'react-router-dom'
import lion from '../assets/img/lion.png';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from "react-bootstrap";

class MainNav extends Component {
  render() {
    return (
      <Navbar id="MainNav" inverse collapseOnSelect fluid fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <Link className="navbar-brand" to="/">
              <img src={lion} alt="Woodstock Lions" height="25px" width="auto"/>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavDropdown eventKey={1} title="Woodstock Sites" id="nav-dropdown-1">
              <MenuItem eventKey={1.1} href="http://woodstockelementary.com">Woodstock Classroom Sites</MenuItem>
              <MenuItem eventKey={1.2} href="http://pps.net/woodstock">Woodstock School (PPS Site)</MenuItem>
              <MenuItem eventKey={1.3} href="http://woodstock-pta.org">Woodstock PTA</MenuItem>
              <MenuItem eventKey={1.4} href="http://www.shurenofportland.org">Shu Ren</MenuItem>
            </NavDropdown>
          </Nav>
          <Nav pullRight>
            {this.props.currentUser && (
              <NavItem eventKey={2} onClick={this.props.onLogout}>Logout</NavItem>
            )}

            {/*<NavDropdown eventKey={3} title="Hello, World" id="basic-nav-dropdown">
              <MenuItem eventKey={3.1}>Profile</MenuItem>
              <MenuItem eventKey={3.2}>Request Correction</MenuItem>
              <MenuItem eventKey={3.3}>Provide Feedback</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.3} onClick={this.props.onLogout}>Logout</MenuItem>
            </NavDropdown>*/}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default MainNav;
