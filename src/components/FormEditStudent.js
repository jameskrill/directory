import React, { Component } from 'react';
import jquery from 'jquery';
window.jQuery = jquery;
require('bootstrap/dist/js/bootstrap');

export default class FormEditStudent extends Component {
  constructor(props) {
    super(props);
    this.state = props.student;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onEditStudent(this.state);
  }

  handleChange = (e) => {
    const target = e.target;
    let value = target.type === 'checkbox' ? (target.checked ? "Y" : "N" ) : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form name="requestPasswordForm" onSubmit={this.handleSubmit}>
        <div className="modal-header">
          <h4 className="modal-title" id="adminChangeLabel">Change Information</h4>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <p>
              Use the form below to change the student's information.
            </p>
          </div>
          <div className="student-data-fields">

            <ul className="nav nav-tabs" role="tablist">
              <li role="presentation" className="active">
                <a href="#student-info" aria-controls="student-info" role="tab" data-toggle="tab">Student</a>
              </li>
              <li role="presentation">
                <a href="#class-info" aria-controls="class-info" role="tab" data-toggle="tab">Classroom</a>
              </li>
              <li role="presentation">
                <a href="#parent-info" aria-controls="parent-info" role="tab" data-toggle="tab">Parents</a>
              </li>
              <li role="presentation">
                <a href="#general-info" aria-controls="general-info" role="tab" data-toggle="tab">General</a>
              </li>
            </ul>

            <div className="tab-content">
              <div role="tabpanel" className="tab-pane active" id="student-info">

                <h3>Student Information</h3>

                <div className="row">
                  {/* student_name */}
                  <div className="form-group col-xs-12">
                    <label htmlFor="student_name" className="control-label">
                      Student Name:
                    </label>
                    <input type="text" name="student_name" id="student_name"
                           className="form-control"
                           value={this.state.student_name}
                           onChange={this.handleChange} />
                    <span id="helpBlock" className="help-block">
                      Comma separated with last name first, like: Smith, John
                    </span>
                  </div>
                </div>

                <div className="row">
                  {/* home_address */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="home_address" className="control-label">
                      Home Address
                    </label>
                    <input type="text" name="home_address" id="home_address"
                      className="form-control"
                      value={this.state.home_address}
                      onChange={this.handleChange} />
                  </div>

                  {/* home_city_state_zip */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="home_city_state_zip" className="control-label">
                      Home City, State, Zip
                    </label>
                    <input type="text" name="home_city_state_zip" id="home_city_state_zip"
                      className="form-control"
                      value={this.state.home_city_state_zip}
                      onChange={this.handleChange} />
                  </div>
                </div>

                <div className="row">
                  {/* mailing_address */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="mailing_address" className="control-label">
                      Mailing Address
                    </label>
                    <input type="text" name="mailing_address" id="mailing_address"
                      className="form-control"
                      value={this.state.mailing_address}
                      onChange={this.handleChange} />
                  </div>

                  {/* mail_city_state_zip */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="mail_city_state_zip" className="control-label">
                      Mailing City, State, Zip
                    </label>
                    <input type="text" name="mail_city_state_zip" id="mail_city_state_zip"
                      className="form-control"
                      value={this.state.mail_city_state_zip}
                      onChange={this.handleChange} />
                  </div>
                </div>

                <div className="row">
                  {/* primary_phone */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="primary_phone" className="control-label">
                      Primary Phone Number
                    </label>
                    <input type="text" name="primary_phone" id="primary_phone"
                      className="form-control"
                      value={this.state.primary_phone}
                      onChange={this.handleChange} />
                  </div>

                  {/* primary_phone_type */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="primary_phone_type" className="control-label">
                      Primary Phone Type
                    </label>
                    <input type="text" name="primary_phone_type" id="primary_phone_type"
                      className="form-control"
                      value={this.state.primary_phone_type}
                      onChange={this.handleChange} />
                  </div>
                </div>

              </div>

              <div role="tabpanel" className="tab-pane" id="class-info">

                <h3>Classroom Information</h3>

                <div className="row">
                  {/* teacher_name */}
                  <div className="form-group col-xs-12">
                    <label htmlFor="teacher_name" className="control-label">
                      Teacher Name
                    </label>
                    <input type="text" name="teacher_name" id="teacher_name"
                      className="form-control"
                      value={this.state.teacher_name}
                      onChange={this.handleChange} />
                  </div>
                </div>

                <div className="row">
                  {/* room */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="room" className="control-label">
                      Room #
                    </label>
                    <input type="text" name="room" id="room"
                      className="form-control"
                      value={this.state.room}
                      onChange={this.handleChange} />
                  </div>
                  {/* grade */}
                  <div className="form-group col-xs-12 col-sm-6">
                    <label htmlFor="grade" className="control-label">
                      Grade
                    </label>
                    <select id="grade" name="grade" className="form-control"
                            value={this.state.grade}
                            onChange={this.handleChange}>
                      <option value="KG">Kindergarten</option>
                      <option value="01">1st Grade</option>
                      <option value="02">2nd Grade</option>
                      <option value="03">3rd Grade</option>
                      <option value="04">4th Grade</option>
                      <option value="05">5th Grade</option>
                    </select>
                  </div>
                </div>

              </div>

              <div role="tabpanel" className="tab-pane" id="parent-info">

                <ul className="nav nav-pills" role="tablist">
                  <li role="presentation" className="active">
                    <a href="#parent-1" aria-controls="parent-1" role="tab" data-toggle="tab">Parent 1</a>
                  </li>
                  <li role="presentation">
                    <a href="#parent-2" aria-controls="parent-2" role="tab" data-toggle="tab">Parent 2</a>
                  </li>
                  <li role="presentation">
                    <a href="#parent-3" aria-controls="parent-3" role="tab" data-toggle="tab">Parent 3</a>
                  </li>
                </ul>

                <div className="tab-content">

                  <div role="tabpanel" className="tab-pane active" id="parent-1">

                    <h3>Parent 1 Information</h3>

                    <div className="row">
                      {/* p1_first_name */}
                      <div className="form-group col-xs-12 col-sm-6">
                        <label htmlFor="p1_first_name" className="control-label">
                          First Name
                        </label>
                        <input type="text" name="p1_first_name" id="p1_first_name"
                          className="form-control"
                          value={this.state.p1_first_name}
                          onChange={this.handleChange} />
                      </div>
                      {/* p1_last_name */}
                      <div className="form-group col-xs-12 col-sm-6">
                        <label htmlFor="p1_last_name" className="control-label">
                          Last Name
                        </label>
                        <input type="text" name="p1_last_name" id="p1_last_name"
                          className="form-control"
                          value={this.state.p1_last_name}
                          onChange={this.handleChange} />
                      </div>
                    </div>
                    {/* p1_relation */}
                    <div className="form-group">
                      <label htmlFor="p1_relation" className="control-label">
                        Relation to Student
                      </label>
                      <input type="text" name="p1_relation" id="p1_relation"
                        className="form-control"
                        value={this.state.p1_relation}
                        onChange={this.handleChange} />
                    </div>
                    <div className="row">
                      {/* p1_email */}
                      <div className="form-group col-xs-12 col-sm-6">
                        <label htmlFor="p1_email" className="control-label">
                          Email
                        </label>
                        <input type="text" name="p1_email" id="p1_email"
                          className="form-control"
                          value={this.state.p1_email}
                          onChange={this.handleChange} />
                      </div>
                      {/* p1_phone */}
                      <div className="form-group col-xs-12 col-sm-6">
                        <label htmlFor="p1_phone" className="control-label">
                          Phone
                        </label>
                        <input type="text" name="p1_phone" id="p1_phone"
                          className="form-control"
                          value={this.state.p1_phone}
                          onChange={this.handleChange} />
                      </div>
                    </div>
                    {/* p1_phone_type */}
                    <div className="form-group">
                      <label htmlFor="p1_phone_type" className="control-label">
                        Phone Type
                      </label>
                      <input type="text" name="p1_phone_type" id="p1_phone_type"
                        className="form-control"
                        value={this.state.p1_phone_type}
                        onChange={this.handleChange} />
                    </div>
                    {/* p1_address */}
                    <div className="form-group">
                      <label htmlFor="p1_address" className="control-label">
                        Address
                      </label>
                      <input type="text" name="p1_address" id="p1_address"
                        className="form-control"
                        value={this.state.p1_address}
                        onChange={this.handleChange} />
                    </div>
                    <div className="row">
                      {/* p1_city */}
                      <div className="form-group col-xs-12 col-sm-4">
                        <label htmlFor="p1_city" className="control-label">
                          City
                        </label>
                        <input type="text" name="p1_city" id="p1_city"
                          className="form-control"
                          value={this.state.p1_city}
                          onChange={this.handleChange} />
                      </div>
                      {/* p1_state */}
                      <div className="form-group col-xs-12 col-sm-4">
                        <label htmlFor="p1_state" className="control-label">
                          State
                        </label>
                        <input type="text" name="p1_state" id="p1_state"
                          className="form-control"
                          value={this.state.p1_state}
                          onChange={this.handleChange} />
                      </div>
                      {/* p1_zip */}
                      <div className="form-group col-xs-12 col-sm-4">
                        <label htmlFor="p1_zip" className="control-label">
                          Zip
                        </label>
                        <input type="text" name="p1_zip" id="p1_zip"
                          className="form-control"
                          value={this.state.p1_zip}
                          onChange={this.handleChange} />
                      </div>
                    </div>

                    <h4>Parent 1 Settings</h4>

                    {/* p1_contact_allowed */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_contact_allowed"
                               id="p1_contact_allowed"
                               checked={this.state.p1_contact_allowed === "Y" ? true : false}
                               onChange={this.handleChange} /> Contact Allowed
                      </label>
                    </div>

                    {/* p1_mailings_allowed */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_mailings_allowed"
                               id="p1_mailings_allowed"
                               checked={this.state.p1_mailings_allowed === "Y" ? true : false}
                               onChange={this.handleChange} /> Mailings Allowed
                      </label>
                    </div>

                    {/* p1_lives_with */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_lives_with"
                               id="p1_lives_with"
                               checked={this.state.p1_lives_with === "Y" ? true : false}
                               onChange={this.handleChange} /> Lives With
                      </label>
                    </div>

                    {/* p1_exclude_address */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_exclude_address"
                               id="p1_exclude_address"
                               checked={this.state.p1_exclude_address === "Y" ? true : false}
                               onChange={this.handleChange} /> Exclude Address
                      </label>
                    </div>

                    {/* p1_exclude_email */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_exclude_email"
                               id="p1_exclude_email"
                               checked={this.state.p1_exclude_email === "Y" ? true : false}
                               onChange={this.handleChange} /> Exclude Email
                      </label>
                    </div>

                    {/* p1_exclude_phone */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_exclude_phone"
                               id="p1_exclude_phone"
                               checked={this.state.p1_exclude_phone === "Y" ? true : false}
                               onChange={this.handleChange} /> Exclude Phone
                      </label>
                    </div>

                    {/* p1_exclude_name */}
                    <div className="checkbox">
                      <label>
                        <input type="checkbox"
                               name="p1_exclude_name"
                               id="p1_exclude_name"
                               checked={this.state.p1_exclude_name === "Y" ? true : false}
                               onChange={this.handleChange} /> Exclude Name
                      </label>
                    </div>

                  </div>

                  <div role="tabpanel" className="tab-pane" id="parent-2">

                    <h3>Parent 2 Information</h3>

                      <div className="row">
                        {/* p2_first_name */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p2_first_name" className="control-label">
                            First Name
                          </label>
                          <input type="text" name="p2_first_name" id="p2_first_name"
                            className="form-control"
                            value={this.state.p2_first_name}
                            onChange={this.handleChange} />
                        </div>
                        {/* p2_last_name */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p2_last_name" className="control-label">
                            Last Name
                          </label>
                          <input type="text" name="p2_last_name" id="p2_last_name"
                            className="form-control"
                            value={this.state.p2_last_name}
                            onChange={this.handleChange} />
                        </div>
                      </div>
                      {/* p2_relation */}
                      <div className="form-group">
                        <label htmlFor="p2_relation" className="control-label">
                          Relation to Student
                        </label>
                        <input type="text" name="p2_relation" id="p2_relation"
                          className="form-control"
                          value={this.state.p2_relation}
                          onChange={this.handleChange} />
                      </div>
                      <div className="row">
                        {/* p2_email */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p2_email" className="control-label">
                            Email
                          </label>
                          <input type="text" name="p2_email" id="p2_email"
                            className="form-control"
                            value={this.state.p2_email}
                            onChange={this.handleChange} />
                        </div>
                        {/* p2_phone */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p2_phone" className="control-label">
                            Phone
                          </label>
                          <input type="text" name="p2_phone" id="p2_phone"
                            className="form-control"
                            value={this.state.p2_phone}
                            onChange={this.handleChange} />
                        </div>
                      </div>
                      {/* p2_phone_type */}
                      <div className="form-group">
                        <label htmlFor="p2_phone_type" className="control-label">
                          Phone Type
                        </label>
                        <input type="text" name="p2_phone_type" id="p2_phone_type"
                          className="form-control"
                          value={this.state.p2_phone_type}
                          onChange={this.handleChange} />
                      </div>
                      {/* p2_address */}
                      <div className="form-group">
                        <label htmlFor="p2_address" className="control-label">
                          Address
                        </label>
                        <input type="text" name="p2_address" id="p2_address"
                          className="form-control"
                          value={this.state.p2_address}
                          onChange={this.handleChange} />
                      </div>
                      <div className="row">
                        {/* p2_city */}
                        <div className="form-group col-xs-12 col-sm-4">
                          <label htmlFor="p2_city" className="control-label">
                            City
                          </label>
                          <input type="text" name="p2_city" id="p2_city"
                            className="form-control"
                            value={this.state.p2_city}
                            onChange={this.handleChange} />
                        </div>
                        {/* p2_state */}
                        <div className="form-group col-xs-12 col-sm-4">
                          <label htmlFor="p2_state" className="control-label">
                            State
                          </label>
                          <input type="text" name="p2_state" id="p2_state"
                            className="form-control"
                            value={this.state.p2_state}
                            onChange={this.handleChange} />
                        </div>
                        {/* p2_zip */}
                        <div className="form-group col-xs-12 col-sm-4">
                          <label htmlFor="p2_zip" className="control-label">
                            Zip
                          </label>
                          <input type="text" name="p2_zip" id="p2_zip"
                            className="form-control"
                            value={this.state.p2_zip}
                            onChange={this.handleChange} />
                        </div>
                      </div>

                      <h4>Parent 2 Settings</h4>

                      {/* p2_contact_allowed */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_contact_allowed"
                                 id="p2_contact_allowed"
                                 checked={this.state.p2_contact_allowed === "Y" ? true : false}
                                 onChange={this.handleChange} /> Contact Allowed
                        </label>
                      </div>

                      {/* p2_mailings_allowed */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_mailings_allowed"
                                 id="p2_mailings_allowed"
                                 checked={this.state.p2_mailings_allowed === "Y" ? true : false}
                                 onChange={this.handleChange} /> Mailings Allowed
                        </label>
                      </div>

                      {/* p2_lives_with */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_lives_with"
                                 id="p2_lives_with"
                                 checked={this.state.p2_lives_with === "Y" ? true : false}
                                 onChange={this.handleChange} /> Lives With
                        </label>
                      </div>

                      {/* p2_exclude_address */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_exclude_address"
                                 id="p2_exclude_address"
                                 checked={this.state.p2_exclude_address === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Address
                        </label>
                      </div>

                      {/* p2_exclude_email */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_exclude_email"
                                 id="p2_exclude_email"
                                 checked={this.state.p2_exclude_email === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Email
                        </label>
                      </div>

                      {/* p2_exclude_phone */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_exclude_phone"
                                 id="p2_exclude_phone"
                                 checked={this.state.p2_exclude_phone === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Phone
                        </label>
                      </div>

                      {/* p2_exclude_name */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p2_exclude_name"
                                 id="p2_exclude_name"
                                 checked={this.state.p2_exclude_name === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Name
                        </label>
                      </div>

                  </div>

                  <div role="tabpanel" className="tab-pane" id="parent-3">

                    <h3>Parent 3 Information</h3>

                      <div className="row">
                        {/* p3_first_name */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p3_first_name" className="control-label">
                            First Name
                          </label>
                          <input type="text" name="p3_first_name" id="p3_first_name"
                            className="form-control"
                            value={this.state.p3_first_name}
                            onChange={this.handleChange} />
                        </div>
                        {/* p3_last_name */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p3_last_name" className="control-label">
                            Last Name
                          </label>
                          <input type="text" name="p3_last_name" id="p3_last_name"
                            className="form-control"
                            value={this.state.p3_last_name}
                            onChange={this.handleChange} />
                        </div>
                      </div>
                      {/* p3_relation */}
                      <div className="form-group">
                        <label htmlFor="p3_relation" className="control-label">
                          Relation to Student
                        </label>
                        <input type="text" name="p3_relation" id="p3_relation"
                          className="form-control"
                          value={this.state.p3_relation}
                          onChange={this.handleChange} />
                      </div>
                      <div className="row">
                        {/* p3_email */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p3_email" className="control-label">
                            Email
                          </label>
                          <input type="text" name="p3_email" id="p3_email"
                            className="form-control"
                            value={this.state.p3_email}
                            onChange={this.handleChange} />
                        </div>
                        {/* p3_phone */}
                        <div className="form-group col-xs-12 col-sm-6">
                          <label htmlFor="p3_phone" className="control-label">
                            Phone
                          </label>
                          <input type="text" name="p3_phone" id="p3_phone"
                            className="form-control"
                            value={this.state.p3_phone}
                            onChange={this.handleChange} />
                        </div>
                      </div>
                      {/* p3_phone_type */}
                      <div className="form-group">
                        <label htmlFor="p3_phone_type" className="control-label">
                          Phone Type
                        </label>
                        <input type="text" name="p3_phone_type" id="p3_phone_type"
                          className="form-control"
                          value={this.state.p3_phone_type}
                          onChange={this.handleChange} />
                      </div>
                      {/* p3_address */}
                      <div className="form-group">
                        <label htmlFor="p3_address" className="control-label">
                          Address
                        </label>
                        <input type="text" name="p3_address" id="p3_address"
                          className="form-control"
                          value={this.state.p3_address}
                          onChange={this.handleChange} />
                      </div>
                      <div className="row">
                        {/* p3_city */}
                        <div className="form-group col-xs-12 col-sm-4">
                          <label htmlFor="p3_city" className="control-label">
                            City
                          </label>
                          <input type="text" name="p3_city" id="p3_city"
                            className="form-control"
                            value={this.state.p3_city}
                            onChange={this.handleChange} />
                        </div>
                        {/* p3_state */}
                        <div className="form-group col-xs-12 col-sm-4">
                          <label htmlFor="p3_state" className="control-label">
                            State
                          </label>
                          <input type="text" name="p3_state" id="p3_state"
                            className="form-control"
                            value={this.state.p3_state}
                            onChange={this.handleChange} />
                        </div>
                        {/* p3_zip */}
                        <div className="form-group col-xs-12 col-sm-4">
                          <label htmlFor="p3_zip" className="control-label">
                            Zip
                          </label>
                          <input type="text" name="p3_zip" id="p3_zip"
                            className="form-control"
                            value={this.state.p3_zip}
                            onChange={this.handleChange} />
                        </div>
                      </div>

                      <h4>Parent 3 Settings</h4>

                      {/* p3_contact_allowed */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_contact_allowed"
                                 id="p3_contact_allowed"
                                 checked={this.state.p3_contact_allowed === "Y" ? true : false}
                                 onChange={this.handleChange} /> Contact Allowed
                        </label>
                      </div>

                      {/* p3_mailings_allowed */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_mailings_allowed"
                                 id="p3_mailings_allowed"
                                 checked={this.state.p3_mailings_allowed === "Y" ? true : false}
                                 onChange={this.handleChange} /> Mailings Allowed
                        </label>
                      </div>

                      {/* p3_lives_with */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_lives_with"
                                 id="p3_lives_with"
                                 checked={this.state.p3_lives_with === "Y" ? true : false}
                                 onChange={this.handleChange} /> Lives With
                        </label>
                      </div>

                      {/* p3_exclude_address */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_exclude_address"
                                 id="p3_exclude_address"
                                 checked={this.state.p3_exclude_address === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Address
                        </label>
                      </div>

                      {/* p3_exclude_email */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_exclude_email"
                                 id="p3_exclude_email"
                                 checked={this.state.p3_exclude_email === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Email
                        </label>
                      </div>

                      {/* p3_exclude_phone */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_exclude_phone"
                                 id="p3_exclude_phone"
                                 checked={this.state.p3_exclude_phone === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Phone
                        </label>
                      </div>

                      {/* p3_exclude_name */}
                      <div className="checkbox">
                        <label>
                          <input type="checkbox"
                                 name="p3_exclude_name"
                                 id="p3_exclude_name"
                                 checked={this.state.p3_exclude_name === "Y" ? true : false}
                                 onChange={this.handleChange} /> Exclude Name
                        </label>
                      </div>

                  </div>

                </div>


              </div>
              <div role="tabpanel" className="tab-pane" id="general-info">

                <h3>General Information</h3>
                <div className="checkbox">
                  <label>
                    <input type="checkbox"
                           name="exclude_from_directory"
                           id="exclude_from_directory"
                           checked={this.state.exclude_from_directory === "Y" ? true : false}
                           onChange={this.handleChange} /> Exclude From Directory
                  </label>
                </div>

                <div className="checkbox">
                  <label>
                    <input type="checkbox"
                           name="exclude_address"
                           id="exclude_address"
                           checked={this.state.exclude_address === "Y" ? true : false}
                           onChange={this.handleChange} /> Exclude Address
                  </label>
                </div>

                <div className="checkbox">
                  <label>
                    <input type="checkbox"
                           name="exclude_email"
                           id="exclude_email"
                           checked={this.state.exclude_email === "Y" ? true : false}
                           onChange={this.handleChange} /> Exclude Email
                  </label>
                </div>

                <div className="checkbox">
                  <label>
                    <input type="checkbox"
                           name="exclude_phone"
                           id="exclude_phone"
                           checked={this.state.exclude_phone === "Y" ? true : false}
                           onChange={this.handleChange} /> Exclude Phone
                  </label>
                </div>

                <div className="checkbox">
                  <label>
                    <input type="checkbox"
                           name="exclude_parents"
                           id="exclude_parents"
                           checked={this.state.exclude_parents === "Y" ? true : false}
                           onChange={this.handleChange} /> Exclude Parents
                  </label>
                </div>

              </div>
            </div>


          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={this.props.closeModal}>Close</button>
          <button type="submit" className="btn btn-primary" id="action" name="action" value="admin_change">Submit</button>
        </div>
      </form>
    )
  }
}
