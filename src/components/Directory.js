import React, {Component} from 'react';
import _ from 'lodash';
import NavBar from './NavBar';
import Search from './Search';
import Students from './Students';
import Login from './Login'
// import WayPoint from "react-waypoint";
import '../assets/styles/Directory.css';

function formatSearchObject(search) {

  var searchObject = {
    "action": "search",
    "where":  []
  };

  if (search.term && search.term !== "") {
    let name = search.term.split(' ');
    searchObject.where[0] = {};
    if (name.length > 1) {
      // This needs to be fixed... I think we need a way to get more specific as they type more names.
      searchObject.where[0]['clause'] = "OR";
      searchObject.where[0]['operator'] = "LIKE";
      searchObject.where[0]['queries'] = [
        {"student_name": '%' + name[1] + ', ' + name[0] + '%'},
        {"p1_first_name": '%' + name[0] + '%', "p1_last_name": '%' + name[1] + '%'},
        {"p2_first_name": '%' + name[0] + '%', "p2_last_name": '%' + name[1] + '%'},
        {"p3_first_name": '%' + name[0] + '%', "p3_last_name": '%' + name[1] + '%'},
      ];
    } else {
      searchObject.where[0]['clause'] = "OR";
      searchObject.where[0]['operator'] = "LIKE";
      searchObject.where[0]['queries'] = [
        {"student_name": '%' + search.term + '%'},
        {"p1_first_name": '%' + search.term + '%'},
        {"p1_last_name": '%' + search.term + '%'},
        {"p2_first_name": '%' + search.term + '%'},
        {"p2_last_name": '%' + search.term + '%'},
        {"p3_first_name": '%' + search.term + '%'},
        {"p3_last_name": '%' + search.term + '%'},
      ];
    }
  }

  if (search.grade_filter && search.grade_filter !== "") {
    searchObject.where[1] = {};
    searchObject.where[1]['clause'] = "AND";
    searchObject.where[1]['operator'] = "=";
    searchObject.where[1]['queries'] = [{
      'grade': search.grade_filter
    }];
  }

  if (search.teacher_filter && search.teacher_filter !== "") {
    searchObject.where[2] = {};
    searchObject.where[2]['clause'] = "AND";
    searchObject.where[2]['operator'] = "=";
    searchObject.where[2]['queries'] = [{
      'teacher_name': search.teacher_filter
    }];
  }

  return searchObject;

}

function transformStudentsArray(students) {
  if (students) {
    let tempStudentsObj = {};

    // loop through students and build students state object
    students.forEach(student => {

      let newLetter = student.student_name.charAt(0).toUpperCase();

      if (!tempStudentsObj[newLetter]) {
        tempStudentsObj[newLetter] = {
          alpha: newLetter,
          students: {}
        };
      }

      if (!tempStudentsObj[newLetter]['students'][student.student_name]) {
        tempStudentsObj[newLetter]['students'][student.student_name] = student;
      }

    });

    return tempStudentsObj;
  }
  return false;
}

class Directory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: {
        term: "",
        teacher_filter: "",
        grade_filter: "",
      },
      teachers: null,
      grades: null,
      students: null,
      sortedStudents: null,
      search_active: false,
      loading: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.currentUser) {
      this.handleClearFilters();
    }
  }

  handleSearch = (search) => {

    this.setState({ loading: true });

    // Return early if we are resetting search.
    if (!search || ( search.term === "" && !search.teacher_filter && !search.grade_filter ) ) {
      this.handleClearFilters();
      return;
    }

    let data = formatSearchObject(search);

    // Handle Search
    fetch('http://directory.woodstockelementary.com/data/post_data.php', {
      method: 'POST',
      headers: new Headers(),
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(
        (result) => {
          let sortedStudents = transformStudentsArray(result);
          this.setState({
            search: search,
            students: result,
            sortedStudents: sortedStudents,
            search_active: true,
            loading: false
          });
        },
        (error) => {
          alert("There was an error with the search. Check the console.");
          this.setState({
            loading: false
          });
        }
      );

  }

  handleClearFilters = () => {
    this.setState({
      search: {
        term: "",
        teacher_filter: "",
        grade_filter: ""
      },
      students: null,
      sortedStudents: null,
      search_active: false,
      loading: false
    });
  }

  render() {

    const debouncedSearch = _.debounce((search) => {this.handleSearch(search)}, 1000);
    return (
      <div className="directory--container">
        {this.state.students && (
          <NavBar sortedStudents={this.state.sortedStudents} />
        )}

        {this.props.currentUser && (
          <div>
            <Search search_active={this.state.search_active}
                onSearch={this.handleSearch}
                onDebouncedSearch={debouncedSearch}
                onClearFilters={this.handleClearFilters}/>
            <Students students={this.state.students}
                  sortedStudents={this.state.sortedStudents}
                  search={this.state.search}
                  currentUser={this.props.currentUser}
                  loading={this.state.loading} />
          </div>
        )}

        {!this.props.currentUser && (
          <Login onLogin={this.props.onLogin} />
        )}

      </div>
    )
  }

}

export default Directory;
