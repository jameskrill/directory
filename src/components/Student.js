import React, { Component } from 'react';
import axios from 'axios';
import Parent from './Parent';
import ModalRequestChange from './ModalRequestChange';
import ModalEditStudent from './ModalEditStudent';
import jQuery from 'jquery';
import { reverseName, gradeMap } from '../helpers/helper_functions';

class Student extends Component {
  constructor(props) {
    super(props);

    let student = props.student;
    let parents = this.rebuildParents(student);

    this.state = {
      student: student,
      parents: parents
    };

  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.student !== this.state.student) {
      let parents = this.rebuildParents(nextProps.student);
      this.setState({
        parents: parents
      });
    }
  }

  rebuildParents = (student) => {
    let parents = null;

    // build parents
    let parentNumToCheck = 3;
    for(var i = 1; i <= parentNumToCheck; i++) {
      let prefix = "p"+i;
      if (student[prefix+"_last_name"] !== "" && student[prefix+"_exclude_name"] !== "Y") {
        if (!parents) {
          parents = [];
        }
        parents.push({
          name: student[prefix+'_first_name'] + " " + student[prefix+'_last_name'],
          email: student[prefix+'_email'],
          phoneType: student[prefix+'_phone_type'],
          phone: student[prefix+'_phone'],
          address: student[prefix+'_address'],
          cityStateZip: student[prefix+'_city'] + ' ' + student[prefix+'_state'] + ', ' + student[prefix+'_zip'],
          showName: student[prefix+'_exclude_name'] !== "Y",
          showEmail: student[prefix+'_exclude_email'] !== "Y" && student['exclude_email'] !== "Y",
          showAddress: student[prefix+'_exclude_address'] !== "Y" && student['exclude_address'] !== "Y",
          showPhone: student[prefix+'_exclude_phone'] !== "Y" && student['exclude_phone'] !== "Y"
        });
      }
    }

    return parents;
  }

  handleClickShowAdults = (e) => {
    e.preventDefault();
    var btn = jQuery(e.target);
    var icon = btn.find('i');
    var parent = btn.parents('.student');
    var guardians = parent.find('.student-guardians');
    guardians.slideToggle();
    icon.toggleClass('fa-plus');
    icon.toggleClass('fa-minus');
  }

  handleEditStudent = (student) => {

    // If there were changes, submit them
    if (student !== this.state.student) {
      axios({
        url: 'http://directory.woodstockelementary.com/data/post_data.php',
        method: 'post',
        data: {
          action: 'update_student_data',
          studentData: student
        }
      }).then(response => {
        // update state here
        if (response.data.success) {
          let parents = this.rebuildParents(student);
          this.setState({student: student, parents: parents});
        } else {
          alert("There was an error trying to update the student. Contact the system admin.");
        }
      }).catch(error => {
        alert("There was an error trying to update the student. Try again.");
      });
    }
  }


  render() {
    const student = this.state.student;
    const parents = this.state.parents;

    return (
      <div className={`student`}>
        <div className={`student-heading row`}>
          <div className="col-xs-12 col-md-7">
            <h3 className="student-name">
              {reverseName(student.student_name)}
              <span className="student-meta">{gradeMap(student.grade)} - {reverseName(student.teacher_name)} - {student.room}</span>
            </h3>
          </div>
          <div className="col-xs-12 col-md-5 text-right">
            {student.exclude_address !== "Y" && (
              <span className="student-primary-contact">
                {student.home_address}
              </span>
            )}

            {student.exclude_phone !== "Y" && (
              <a href={`tel:${student.primary_phone}`} className="primary-phone">
                {student.primary_phone}
              </a>
            )}

          </div>
        </div>

        {student.exclude_parents !== "Y" && parents && (
          <div>
            <div className="student-guardians row">
              {parents.map(parent =>
                <Parent key={parent.name} parent={parent} />
              )}
            </div>
            <div className="guardians-toggle visible-xs row">
              <div className="col-xs-12">
                <button className="guardian-toggle btn btn-link" onClick={this.handleClickShowAdults}>
                  <i className="fa fa-plus" /> Show Adult Contacts
                </button>
              </div>
            </div>
          </div>
        )}

        <div className="meta-links row">
          <div className="col-xs-6">
            <ModalRequestChange student={student} />
            {this.props.currentUser.userRole && this.props.currentUser.userRole === "admin" && (
              <ModalEditStudent student={student} onEditStudent={this.handleEditStudent} />
            )}
          </div>
          <div className="col-xs-6 text-right">
            <p>
              <a href="#search" className="btn btn-link">
                <i className="fa fa-arrow-circle-up" /> Back to Search
              </a>
            </p>
          </div>
        </div>

      </div>
    )
  }
}

export default Student;
