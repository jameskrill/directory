import React, { Component } from 'react';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

import MainNav from './MainNav';
import Directory from './Directory';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: null
    };
  }

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  componentWillMount() {
    const { cookies } = this.props;

    this.setState({
      currentUser: cookies.get('wsd_currentUser') || null
    });
  }

  handleLogin = (currentUser, rememberMe) => {
    if (rememberMe) {
      const { cookies } = this.props;
      cookies.set('wsd_currentUser', currentUser, { path: '/' });
    }

    this.setState({
      currentUser: currentUser
    });
  }

  handleLogout = (e) => {
    e.preventDefault();
    const { cookies } = this.props;
    cookies.remove('wsd_currentUser');
    this.setState({
      currentUser: null
    });
  }

  render() {
    return (
      <div>
        <MainNav currentUser={this.state.currentUser} onLogout={this.handleLogout} />
        <Directory currentUser={this.state.currentUser} onLogin={this.handleLogin} />
      </div>
    )
  }
}

export default withCookies(App);
