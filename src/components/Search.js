import React, { Component } from "react";
import '../assets/styles/Search.css';
import { gradeMap } from '../helpers/helper_functions';
// import _ from "lodash";
// import $ from "jquery";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: {
        term: "",
        teacher_filter: "",
        grade_filter: ""
      },
      teachers: JSON.parse(localStorage.getItem('wsd_teachers')),
      grades: JSON.parse(localStorage.getItem('wsd_grades'))
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.clearFilters = this.clearFilters.bind(this);
  }

  componentDidMount() {

    // Fetch Teachers
    if (!this.state.teachers) {
    fetch('http://directory.woodstockelementary.com/data/get_teachers.php')
      .then(res => res.json())
      .then(
        (result) => {
          localStorage.setItem('wsd_teachers', JSON.stringify(result));
          this.setState({teachers: result});
        },
        (error) => {
          alert("There was an error loading teachers. Check the console.");
        }
      );
    }

    // Fetch Grades
    if (!this.state.grades) {
    fetch('http://directory.woodstockelementary.com/data/get_grades.php')
      .then(res => res.json())
      .then(
        (result) => {
          let lastItem = result.pop();
          result.unshift(lastItem);
          localStorage.setItem('wsd_grades', JSON.stringify(result));
          this.setState({grades: result});
        },
        (error) => {
          alert("There was an error loading grade levels. Check the console.");
        }
      );
    }
  }

  handleInputChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    let search = {...this.state.search};

    // toggle filters (so they can't both be used)
    if (name === "grade_filter") {
      search.teacher_filter = "";
    }
    if (name === "teacher_filter") {
      search.grade_filter = "";
    }

    // Update with new value
    search[name] = value;

    this.setState({search: search});

    if (name==="term") {
      this.props.onDebouncedSearch(search);
    } else {
      this.props.onSearch(search);
    }

  }

  handleSubmit(e) {
    e.preventDefault();
  }

  clearFilters(e) {
    e.preventDefault();
    this.setState({search: {term: "", teacher_filter: "", grade_filter: ""}});
    this.props.onClearFilters();
  }

  render() {

    // From local state
    const teachers = this.state.teachers;
    const grades = this.state.grades;

    // Pulled from Directory state
    const search_active = this.props.search_active;
    let search_results_class = search_active ? ' hasResults' : '';

    return(
      <div id="search" className={`search-container ${search_results_class}`}>
        <div className="container search">
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-sm-offset-3">
              <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-xs-12">
                    <h2 className="app-title">
                      Woodstock <span className="hidden-xs hidden-sm">School&nbsp;</span>
                      Online Directory <span className="meta">2017 - 2018</span>
                    </h2>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12">
                    <label htmlFor="search" className="sr-only">Search</label>
                    <p>
                      <input type="text" id="search" name="term"
                              placeholder="Search by child, parent or family name..."
                              className="form-control input-lg"
                              value={this.state.search.term}
                              onChange={this.handleInputChange}/>
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-5">
                    <p>
                      <select name="teacher_filter"
                              id="filter-class"
                              className="form-control input-sm"
                              value={this.state.search.teacher_filter}
                              onChange={this.handleInputChange}>
                        <option value="">Filter by Teacher</option>
                        {teachers && teachers.map(teacher => (
                          <option value={teacher} key={teacher}>
                            {teacher}
                          </option>
                        ))}
                      </select>
                    </p>
                  </div>
                  <div className="col-xs-2">
                    <p className="between-filters">OR</p>
                  </div>
                  <div className="col-xs-5">
                    <p>
                      <select name="grade_filter"
                              id="filter-grade"
                              className="form-control input-sm"
                              value={this.state.search.grade_filter}
                              onChange={this.handleInputChange}>
                        <option value="">Filter by Grade</option>
                        {grades && grades.map(grade => (
                          <option value={grade} key={grade}>
                            {gradeMap(grade)}
                          </option>
                        ))}
                      </select>
                    </p>
                  </div>
                </div>
                {search_active && (
                  <div className="row">
                    <div className="col-xs-12">
                      <p className="clear-search pull-right">
                          <button type="reset"
                                  onClick={this.clearFilters}
                                  className="btn btn-sm btn-danger">Clear
                          Filters</button>
                      </p>
                    </div>
                  </div>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}


export default Search;
