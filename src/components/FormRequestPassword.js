import React, { Component } from 'react';
import axios from 'axios';
import jQuery from 'jquery';

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export default class FormRequestPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      action: 'request_password',
      email: '',
      sending: false
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    if (!validateEmail(this.state.email)) {
      alert('You must enter a valid email address...');
      return;
    }

    this.setState({sending: true});

    axios({
      url: 'http://directory.woodstockelementary.com/assets/php/mail.php',
      method: 'post',
      data: this.state
    }).then(response => {
      let form = jQuery('#requestPasswordForm');
      form.find('.modal-body').html('<p>' + response.data + '</p>');
      form.find('.modal-footer #action, .modal-header p').hide();
    }).catch(error => {
      alert('There was an error sending this request. Please try again later.');
      this.setState({sending: false});
    });
  }

  handleInputChange = (e) => {
    // validation?
    this.setState({email: e.target.value});
  }

  render() {
    return (
      <form id="requestPasswordForm" name="requestPasswordForm" onSubmit={this.handleSubmit}>
        <div className="modal-header">
          <h4 className="modal-title" id="exampleModalLabel">Request Password</h4>
          <p>
            Please enter your email address.
          </p>
          <p>
            <strong>If your email is in the directory,</strong> the password will be sent to you automatically.
          </p>
          <p>
            <strong>If your email is NOT in the directory,</strong> an email will be sent to the administration and they will confirm your identification and send you the password.
          </p>
        </div>
        <div className="modal-body">
          {!this.state.sending && (
            <div className="form-group">
              <label htmlFor="email" className="control-label">Email Address:</label>
              <input type="text"
                     className="form-control"
                     id="email"
                     name="email"
                     value={this.state.email}
                     onChange={this.handleInputChange} />
            </div>
          )}
          {this.state.sending && (
            <div className="loading-container">
              <div className="loader" />
            </div>
          )}

        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={this.props.onCloseModal}>Close</button>
          {!this.state.sending && (
            <button type="submit" className="btn btn-primary" id="action" name="action" value="email_password">Send Request</button>
          )}
        </div>
      </form>
    )
  }
}