import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import FormEditStudent from './FormEditStudent';

export default class ModalEditStudent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  handleOpenModal = () => {
    this.setState({open: true});
  }

  handleCloseModal = () => {
    this.setState({ open: false });
  }

  handleEditStudent = (student) => {
    this.handleCloseModal();
    this.props.onEditStudent(student);
  }

  render() {
    const { open } = this.state;
    return (
      <div>
        <p>
          <button className="btn btn-danger" onClick={this.handleOpenModal}>
            <i className="fa fa-pencil" /> Edit Student Data
          </button>
        </p>
        <Modal open={open} onClose={this.handleCloseModal} styles={{overlay: {zIndex: 2040}}}>
          <FormEditStudent student={this.props.student} closeModal={this.handleCloseModal} onEditStudent={this.handleEditStudent} />
        </Modal>
      </div>
    )
  }
}
